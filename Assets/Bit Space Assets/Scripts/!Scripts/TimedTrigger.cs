﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class TimedTrigger : MonoBehaviour {
	public bool triggerAtStart = false;

	public UnityEvent immediateEvents;

	[System.Serializable]
	public class TimerEvent {
		public float timeToWait = 1f;
		public UnityEvent events;
	}
	public TimerEvent[] delayedEvents;

	void Start() {
		if (triggerAtStart) {
			this.Trigger();
		}
	}

	public void Trigger () {
		immediateEvents.Invoke ();

		foreach(TimerEvent te in delayedEvents) {
			StartCoroutine(processTrigger(te));
		}
	}

	public void CancelTriggers() {
		this.StopAllCoroutines();
	}

	private IEnumerator processTrigger(TimerEvent te) {
		yield return new WaitForSeconds(te.timeToWait);
		te.events.Invoke();
	}
}
