﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(LayoutGroup))]
public class IconCounterHUD: MonoBehaviour {
	public GameObject iconPrefab;

	public int currentCount = 0;
	public int maxCount = 10;

	[Tooltip("Turns on when the count is zero")]
	public RectTransform zeroCountObject;

	public void AddCount(int num) {
		currentCount = Mathf.Clamp (currentCount + num, 0, maxCount);
		UpdateCount ();
	}

	public void SubtractCount(int num) {
		currentCount = Mathf.Clamp (currentCount - num, 0, maxCount);
		UpdateCount ();
	}

	public void SetCount(int num) {
		currentCount = Mathf.Clamp (num, 0, maxCount);
		UpdateCount ();
	}

	public void SetMaxCount(int newMax) {
		maxCount = Mathf.Max (newMax, 0);
	}

	private void UpdateCount() {
		StartCoroutine (CountUpdater ());
	}

	private IEnumerator CountUpdater() {
		yield return new WaitForEndOfFrame ();

		currentCount = Mathf.Clamp (currentCount, 0, maxCount);

		List<CountableIcon> iconList = new List<CountableIcon> ();
		this.GetComponentsInChildren<CountableIcon>(iconList);

		if (this.GetComponent<CountableIcon> () != null) {
			iconList.Remove (this.GetComponent<CountableIcon>());
		}

		int iconsCount = iconList.Count;

		/*while ( iconsCount > 0 && iconsCount > currentCount) {
			#if UNITY_EDITOR
			DestroyImmediate(iconList[iconsCount-1].gameObject);
			#elif
			Destroy(iconList[iconsCount-1].gameObject);
			#endif

			iconsCount--;
		}*/

		while (iconsCount < currentCount) {
			GameObject newHeart = Instantiate(iconPrefab) as GameObject;
			newHeart.AddComponent<CountableIcon> ();
			newHeart.transform.SetParent(this.transform, false);
			iconsCount++;
		}

		if (zeroCountObject != null) {
			if (currentCount == 0) {
				zeroCountObject.gameObject.SetActive (true);
			} else {
				zeroCountObject.gameObject.SetActive (false);
			}
		}
	}

	#if UNITY_EDITOR
	void OnValidate() {
		if (this.gameObject.activeInHierarchy) {
			this.UpdateCount ();
		}
	}
	#endif
}
