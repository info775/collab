﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KeyboardEventTrigger : MonoBehaviour {
	public KeyCode key;
	public UnityEvent keyDownEvents;
	public UnityEvent keyUpEvents;

	void Update() {
		if (Input.GetKeyDown (key)) {
			keyDownEvents.Invoke ();
		}

		if (Input.GetKeyUp (key)) {
			keyUpEvents.Invoke ();
		}
	}

}
