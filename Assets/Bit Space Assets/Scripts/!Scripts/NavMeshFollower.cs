﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class NavMeshFollower : MonoBehaviour {
	public Transform thingToFollow;
	private NavMeshAgent myAgent;

	// Use this for initialization
	void Awake () {
		myAgent = this.GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if (thingToFollow == null || !thingToFollow.gameObject.activeInHierarchy) {
			myAgent.isStopped = true;
		} else {
			myAgent.isStopped = false;
			myAgent.SetDestination (thingToFollow.position);
		}
	}
}
