﻿using UnityEngine;
using System.Collections;

public class BoatEngine : MonoBehaviour {

	public float speed = 90f;
	public float turnSpeed = 5f;
	public float buoyancy = 65f;
	public float waterLevel = 7.0f;
	public bool keyboardControl = true;
	public float powerInput;
	public float turnInput;
	public float uprightForce = 1f;
	private Rigidbody boatRigidbody;
	public LayerMask layersToFloatAbove;


	void Awake () 
	{
		boatRigidbody = GetComponent <Rigidbody>();
	}

	void Update () 
	{
		if (keyboardControl) {
			powerInput = Input.GetAxis ("Vertical");
			turnInput = Input.GetAxis ("Horizontal");
		}
	}

	void FixedUpdate()
	{
//		Ray ray = new Ray (transform.position, -transform.up);
//		RaycastHit hit;
//
//		if (Physics.Raycast(ray, out hit, waterLevel, layersToFloatAbove))
//		{
//			float proportionalHeight = (waterLevel - hit.distance) / waterLevel;
//			Vector3 appliedFloatingForce = Vector3.up * proportionalHeight * buoyancy;
//			boatRigidbody.AddForce(appliedFloatingForce, ForceMode.Acceleration);
//		}

		float upAngleDiff = Vector3.Angle (this.transform.up, Vector3.up);
		Vector3 upgrightAxis = Vector3.Cross (this.transform.up, Vector3.up);
		boatRigidbody.AddTorque (upgrightAxis * upAngleDiff * uprightForce, ForceMode.Acceleration);

		Vector3 appliedFloatingForce = Vector3.up * (waterLevel - this.transform.position.y) * buoyancy;
		boatRigidbody.AddForce(appliedFloatingForce, ForceMode.Acceleration);


		boatRigidbody.AddRelativeForce(0f, 0f,powerInput * speed);
		boatRigidbody.AddRelativeTorque(0f,turnInput * turnSpeed,0f);

	}
}